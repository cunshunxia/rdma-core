Summary:       Userspace components for Linux Kernel's drivers/infiniband subsystem
Name:          rdma-core
Version:       47.0
Release:       1%{?dist}
License:       GPLv2 or BSD
Url:           https://github.com/linux-rdma/rdma-core
Source0:       %{url}/releases/download/v%{version}/%{name}-%{version}.tar.gz
Patch3000:     0001-udev-keep-NAME_KERNEL-as-default-interface-naming-co.patch

BuildRequires: binutils gcc pkgconfig pkgconfig(libnl-3.0) pkgconfig(libnl-route-3.0)
BuildRequires: cmake >= 2.8.11
BuildRequires: ninja-build
BuildRequires: libudev-devel /usr/bin/rst2man
BuildRequires: valgrind-devel systemd systemd-devel python3 perl-generators

Requires:      pciutils
Provides:      rdma = %{version}-%{release}
Conflicts:     infiniband-diags <= 1.6.7


%description
This is the userspace components for the Linux Kernel's drivers/infiniband subsystem.
Specifically this contains the userspace libraries for the following device nodes:
/dev/infiniband/uverbsX (libibverbs),/dev/infiniband/rdma_cm (librdmacm),/dev/infiniband/umadX (libibumad).
The userspace component of the libibverbs RDMA kernel drivers are included under the providers/ directory.
Support for the following Kernel RDMA drivers is included: efa.ko,iw_cxgb4.ko,hfi1.ko,hns-roce.ko,
irdma.ko,ib_qib.ko,mlx4_ib.ko,mlx5_ib.ko,ib_mthca.ko,ocrdma.ko,qedr.ko,rdma_rxe.ko,siw.ko,vmw_pvrdma.ko.
Additional service daemons are provided for srp_daemon (ib_srp.ko),iwpmd (for iwarp kernel providers),
ibacm (for InfiniBand communication management assistant).

%package devel
Summary:       RDMA core development libraries and headers
Requires:      libibverbs = %{version}-%{release}
Requires:      libibumad = %{version}-%{release}
Requires:      librdmacm = %{version}-%{release}
Requires:      infiniband-diags = %{version}-%{release}
Provides:      libibverbs-devel = %{version}-%{release}
Provides:      libibumad-devel = %{version}-%{release}
Provides:      librdmacm-devel = %{version}-%{release}
Provides:      ibacm-devel = %{version}-%{release}
Provides:      infiniband-diags-devel = %{version}-%{release}
Provides:      libibmad-devel = %{version}-%{release}

%description devel
RDMA core development libraries and headers.

%package -n infiniband-diags
Summary:      InfiniBand Diagnostic Tools
Requires:     libibumad = %{version}-%{release}
Provides:     perl(IBswcountlimits)
Provides:     libibmad = %{version}-%{release}
Obsoletes:    openib-diags < 1.3

%description -n infiniband-diags
This package provides IB diagnostic programs and scripts to diagnose an
IB subnet, and also provides libibmad, which provides low layer IB functions
for the IB diagnostic and management programs. These include MAD, SA, SMP,
and other basic IB functions.

%package -n infiniband-diags-compat
Summary:      OpenFabrics Alliance InfiniBand Diagnostic Tools

%description -n infiniband-diags-compat
Deprecated scripts and utilities are maintained for compatibility reasons.

%package -n libibverbs
Summary:      Library and drivers for direct userspace use of RDMA (InfiniBand/iWARP/RoCE) hardware
Provides:     libcxgb4 = %{version}-%{release}
Provides:     libefa = %{version}-%{release}
Provides:     libhfi1 = %{version}-%{release}
Provides:     libipathverbs = %{version}-%{release}
Provides:     libirdma = %{version}-%{release}
Provides:     libmlx4 = %{version}-%{release}
Provides:     libmlx5 = %{version}-%{release}
Provides:     libmthca = %{version}-%{release}
Provides:     libocrdma = %{version}-%{release}
Provides:     librxe = %{version}-%{release}

%description -n libibverbs
libibverbs allows userspace processes to use RDMA "verbs" as described
in the InfiniBand Architecture Specification and the RDMA Protocol Verbs Specification,
including direct hardware access from userspace to InfiniBand/iWARP adapters
(kernel bypass) for fast path operations.

Device-specific plug-in ibverbs userspace drivers are included:
- libcxgb4: Chelsio T4 iWARP HCA
- libefa: Amazon Elastic Fabric Adapter
- libhfi1: Intel Omni-Path HFI
- libhns: HiSilicon Hip06 SoC
- libipathverbs: QLogic InfiniPath HCA
- libirdma: Intel Ethernet Connection RDMA
- libmlx4: Mellanox ConnectX-3 InfiniBand HCA
- libmlx5: Mellanox Connect-IB/X-4+ InfiniBand HCA
- libmthca: Mellanox InfiniBand HCA
- libocrdma: Emulex OneConnect RDMA/RoCE Device
- libqedr: QLogic QL4xxx RoCE HCA
- librxe: A software implementation of the RoCE protocol
- libsiw: A software implementation of the iWarp protocol
- libvmw_pvrdma: VMware paravirtual RDMA device

%package -n libibverbs-utils
Summary:      Utilities for libibverbs library
Requires:     libibverbs = %{version}-%{release}

%description -n libibverbs-utils
Utilities using libibverbs library such as ibv_devinfo, which
displays information about RDMA devices.

%package -n ibacm
Summary:      InfiniBand daemon to Assist Communication Manager
%{?systemd_requires}

%description -n ibacm
ibacm daemon helps reduce the load of managing path record lookups on
large InfiniBand fabrics, similar to an ARP cache.  ibacm can reduce
the SA packet load of a large IB cluster from O(n^2) to O(n). 
The ibacm daemon is started and normally runs in the background,
user applications need not know about this daemon as long as their app
uses librdmacm to handle connection bring up/tear down.  The librdmacm
library knows how to talk directly to the ibacm daemon to retrieve data.

%package -n iwpmd
Summary:      iWarp Port Mapper userspace daemon
%{?systemd_requires}

%description -n iwpmd
iwpmd provides a userspace service for iWarp drivers to claim
tcp ports through the standard socket interface.

%package -n libibumad
Summary:      InfiniBand umad (userspace management datagram) library

%description -n libibumad
libibumad provides the userspace management datagram (umad) library
functions, which sit on top of the umad modules in the kernel. These
are used by the IB diagnostic and management tools, including OpenSM.

%package -n librdmacm
Summary:      Userspace RDMA Connection Manager

%description -n librdmacm
Userspace RDMA Communication Management API.

%package -n librdmacm-utils
Summary:      Utilities using librdmacm library
Requires:     librdmacm = %{version}-%{release}

%description -n librdmacm-utils
Utilities and examples with librdmacm library.

%package -n srp_daemon
Summary:      Tools for InfiniBand SRP protocol devices
Provides:     srptools = %{version}-%{release}
Obsoletes:    openib-srptools <= 0.0.6
%{?systemd_requires}

%description -n srp_daemon
srp_daemon allows to discover and use SCSI devices via the SCSI RDMA Protocol
over InfiniBand, conjunctive with the kernel ib_srp driver.


%prep
%autosetup -p1


%build
%cmake -GNinja \
         -DCMAKE_BUILD_TYPE=Release \
         -DCMAKE_INSTALL_BINDIR:PATH=%{_bindir} \
         -DCMAKE_INSTALL_SBINDIR:PATH=%{_sbindir} \
         -DCMAKE_INSTALL_LIBDIR:PATH=%{_libdir} \
         -DCMAKE_INSTALL_LIBEXECDIR:PATH=%{_libexecdir} \
         -DCMAKE_INSTALL_LOCALSTATEDIR:PATH=%{_localstatedir} \
         -DCMAKE_INSTALL_SHAREDSTATEDIR:PATH=%{_sharedstatedir} \
         -DCMAKE_INSTALL_INCLUDEDIR:PATH=%{_includedir} \
         -DCMAKE_INSTALL_INFODIR:PATH=%{_infodir} \
         -DCMAKE_INSTALL_MANDIR:PATH=%{_mandir} \
         -DCMAKE_INSTALL_SYSCONFDIR:PATH=%{_sysconfdir} \
         -DCMAKE_INSTALL_SYSTEMD_SERVICEDIR:PATH=%{_unitdir} \
         -DCMAKE_INSTALL_INITDDIR:PATH=%{_initrddir} \
         -DCMAKE_INSTALL_RUNDIR:PATH=%{_rundir} \
         -DCMAKE_INSTALL_DOCDIR:PATH=%{_docdir}/%{name} \
         -DCMAKE_INSTALL_UDEV_RULESDIR:PATH=%{_udevrulesdir} \
         -DCMAKE_INSTALL_PERLDIR:PATH=%{perl_vendorlib} \
         -DENABLE_IBDIAGS_COMPAT:BOOL=True \
         -DPYTHON_EXECUTABLE:PATH=%{__python3} \
         -DCMAKE_INSTALL_PYTHON_ARCH_LIB:PATH=%{python3_sitearch} \
	 -DNO_PYVERBS=1

ninja-build -C %{_vpath_builddir} -v %{?_smp_mflags}


%install
DESTDIR=%{buildroot} ninja-build -C %{_vpath_builddir} install
rm -rf %{buildroot}/%{_initrddir}/
rm -f %{buildroot}/%{_sbindir}/srp_daemon.sh

install -D -m0644 redhat/rdma.mlx4.conf %{buildroot}/%{_sysconfdir}/rdma/mlx4.conf
rm -f %{buildroot}%{_sysconfdir}/rdma/modules/rdma.conf
install -D -m0644 redhat/rdma.conf %{buildroot}%{_sysconfdir}/rdma/modules/rdma.conf
(cd %{__cmake_builddir};  ./bin/ib_acme -D . -O &&
 install -D -m0644 ibacm_opts.cfg %{buildroot}%{_sysconfdir}/rdma/)

install -D -m0755 redhat/rdma.modules-setup.sh %{buildroot}%{_prefix}/lib/dracut/modules.d/05rdma/module-setup.sh
install -D -m0644 redhat/rdma.mlx4.sys.modprobe %{buildroot}%{_prefix}/lib/modprobe.d/libmlx4.conf
install -D -m0755 redhat/rdma.mlx4-setup.sh %{buildroot}%{_libexecdir}/mlx4-setup.sh


%post -n rdma-core
if [ -x /sbin/udevadm ]; then
/sbin/udevadm trigger --subsystem-match=infiniband --action=change || true
/sbin/udevadm trigger --subsystem-match=net --action=change || true
/sbin/udevadm trigger --subsystem-match=infiniband_mad --action=change || true
fi

%post -n ibacm
%systemd_post ibacm.service
%preun -n ibacm
%systemd_preun ibacm.service
%postun -n ibacm
%systemd_postun_with_restart ibacm.service

%post -n srp_daemon
%systemd_post srp_daemon.service
%preun -n srp_daemon
%systemd_preun srp_daemon.service
%postun -n srp_daemon
%systemd_postun_with_restart srp_daemon.service

%post -n iwpmd
%systemd_post iwpmd.service
%preun -n iwpmd
%systemd_preun iwpmd.service
%postun -n iwpmd
%systemd_postun_with_restart iwpmd.service

%files
%license COPYING.*
%dir %{_sysconfdir}/rdma
%dir %{_docdir}/%{name}
%doc %{_docdir}/%{name}/{README,rxe,udev,tag_matching}.md
%doc %{_docdir}/%{name}/70-persistent-ipoib.rules
%config(noreplace) %{_sysconfdir}/rdma/mlx4.conf
%config(noreplace) %{_sysconfdir}/rdma/modules/infiniband.conf
%config(noreplace) %{_sysconfdir}/rdma/modules/iwarp.conf
%config(noreplace) %{_sysconfdir}/rdma/modules/opa.conf
%config(noreplace) %{_sysconfdir}/rdma/modules/rdma.conf
%config(noreplace) %{_sysconfdir}/rdma/modules/roce.conf

%dir %{_sysconfdir}/modprobe.d
%config(noreplace) %{_sysconfdir}/modprobe.d/mlx4.conf
%config(noreplace) %{_sysconfdir}/modprobe.d/truescale.conf

%{_unitdir}/rdma-hw.target
%{_unitdir}/rdma-load-modules@.service

%dir %{_prefix}/lib/dracut
%dir %{_prefix}/lib/dracut/modules.d
%dir %{_prefix}/lib/dracut/modules.d/05rdma
%{_prefix}/lib/dracut/modules.d/05rdma/module-setup.sh

%dir %{_udevrulesdir}
%{_udevrulesdir}/../rdma_rename
%{_udevrulesdir}/60-rdma-ndd.rules
%{_udevrulesdir}/60-rdma-persistent-naming.rules
%{_udevrulesdir}/75-rdma-description.rules
%{_udevrulesdir}/90-rdma-hw-modules.rules
%{_udevrulesdir}/90-rdma-ulp-modules.rules
%{_udevrulesdir}/90-rdma-umad.rules

%dir %{_prefix}/lib/modprobe.d
%{_prefix}/lib/modprobe.d/libmlx4.conf

%{_libexecdir}/mlx4-setup.sh
%{_libexecdir}/truescale-serdes.cmds
%{_sbindir}/rdma-ndd
%{_unitdir}/rdma-ndd.service
%{_mandir}/man7/rxe*
%{_mandir}/man8/rdma-ndd.*


%files devel
%doc %{_docdir}/%{name}/MAINTAINERS
%dir %{_includedir}/infiniband
%dir %{_includedir}/rdma
%{_includedir}/infiniband/*
%{_includedir}/rdma/*
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_mandir}/man3/efadv*
%{_mandir}/man3/ibv_*
%{_mandir}/man3/rdma*
%{_mandir}/man3/umad*
%{_mandir}/man3/*_to_ibv_rate.*
%{_mandir}/man7/rdma_cm.*
%{_mandir}/man3/mlx5dv*
%{_mandir}/man3/mlx4dv*
%{_mandir}/man7/efadv*
%{_mandir}/man7/mlx5dv*
%{_mandir}/man7/mlx4dv*
%{_mandir}/man3/ibnd_*
%{_mandir}/man3/manadv_*
%{_mandir}/man7/manadv.7.gz


%files -n infiniband-diags
%config(noreplace) %{_sysconfdir}/infiniband-diags/error_thresholds
%config(noreplace) %{_sysconfdir}/infiniband-diags/ibdiag.conf
%{_sbindir}/ibaddr
%{_sbindir}/ibnetdiscover
%{_sbindir}/ibping
%{_sbindir}/ibportstate
%{_sbindir}/ibroute
%{_sbindir}/ibstat
%{_sbindir}/ibsysstat
%{_sbindir}/ibtracert
%{_sbindir}/perfquery
%{_sbindir}/sminfo
%{_sbindir}/smpdump
%{_sbindir}/smpquery
%{_sbindir}/saquery
%{_sbindir}/vendstat
%{_sbindir}/iblinkinfo
%{_sbindir}/ibqueryerrors
%{_sbindir}/ibcacheedit
%{_sbindir}/ibccquery
%{_sbindir}/ibccconfig
%{_sbindir}/dump_fts
%{_sbindir}/ibhosts
%{_sbindir}/ibswitches
%{_sbindir}/ibnodes
%{_sbindir}/ibrouters
%{_sbindir}/ibfindnodesusing.pl
%{_sbindir}/ibidsverify.pl
%{_sbindir}/check_lft_balance.pl
%{_sbindir}/dump_lfts.sh
%{_sbindir}/dump_mfts.sh
%{_sbindir}/ibclearerrors
%{_sbindir}/ibclearcounters
%{_sbindir}/ibstatus
%{_libdir}/libibmad*.so.*
%{_libdir}/libibnetdisc*.so.*
%{perl_vendorlib}/IBswcountlimits.pm
%{_mandir}/man8/ibaddr*
%{_mandir}/man8/ibnetdiscover*
%{_mandir}/man8/ibping*
%{_mandir}/man8/ibportstate*
%{_mandir}/man8/ibroute.*
%{_mandir}/man8/ibstat.*
%{_mandir}/man8/ibsysstat*
%{_mandir}/man8/ibtracert*
%{_mandir}/man8/perfquery*
%{_mandir}/man8/sminfo*
%{_mandir}/man8/smpdump*
%{_mandir}/man8/smpquery*
%{_mandir}/man8/saquery*
%{_mandir}/man8/vendstat*
%{_mandir}/man8/iblinkinfo*
%{_mandir}/man8/ibqueryerrors*
%{_mandir}/man8/ibcacheedit*
%{_mandir}/man8/ibccquery*
%{_mandir}/man8/ibccconfig*
%{_mandir}/man8/dump_fts*
%{_mandir}/man8/ibhosts*
%{_mandir}/man8/ibswitches*
%{_mandir}/man8/ibnodes*
%{_mandir}/man8/ibrouters*
%{_mandir}/man8/ibfindnodesusing*
%{_mandir}/man8/ibidsverify*
%{_mandir}/man8/check_lft_balance*
%{_mandir}/man8/dump_lfts*
%{_mandir}/man8/dump_mfts*
%{_mandir}/man8/ibclearerrors*
%{_mandir}/man8/ibclearcounters*
%{_mandir}/man8/ibstatus*
%{_mandir}/man8/infiniband-diags*


%files -n infiniband-diags-compat
%{_sbindir}/ibcheckerrs
%{_sbindir}/ibchecknet
%{_sbindir}/ibchecknode
%{_sbindir}/ibcheckport
%{_sbindir}/ibcheckportwidth
%{_sbindir}/ibcheckportstate
%{_sbindir}/ibcheckwidth
%{_sbindir}/ibcheckstate
%{_sbindir}/ibcheckerrors
%{_sbindir}/ibdatacounts
%{_sbindir}/ibdatacounters
%{_sbindir}/ibdiscover.pl
%{_sbindir}/ibswportwatch.pl
%{_sbindir}/ibqueryerrors.pl
%{_sbindir}/iblinkinfo.pl
%{_sbindir}/ibprintca.pl
%{_sbindir}/ibprintswitch.pl
%{_sbindir}/ibprintrt.pl
%{_sbindir}/set_nodedesc.sh
%{_mandir}/man8/ibcheckerrs*
%{_mandir}/man8/ibchecknet*
%{_mandir}/man8/ibchecknode*
%{_mandir}/man8/ibcheckport.*
%{_mandir}/man8/ibcheckportwidth*
%{_mandir}/man8/ibcheckportstate*
%{_mandir}/man8/ibcheckwidth*
%{_mandir}/man8/ibcheckstate*
%{_mandir}/man8/ibcheckerrors*
%{_mandir}/man8/ibdatacounts*
%{_mandir}/man8/ibdatacounters*
%{_mandir}/man8/ibdiscover*
%{_mandir}/man8/ibswportwatch*
%{_mandir}/man8/ibprintca*
%{_mandir}/man8/ibprintswitch*
%{_mandir}/man8/ibprintrt*


%files -n libibverbs
%config(noreplace) %{_sysconfdir}/libibverbs.d/*.driver
%dir %{_sysconfdir}/libibverbs.d
%dir %{_libdir}/libibverbs
%{_libdir}/libefa.so.*
%{_libdir}/libibverbs*.so.*
%{_libdir}/libibverbs/*.so
%{_libdir}/libmana.so.*
%{_libdir}/libmlx5.so.*
%{_libdir}/libmlx4.so.*
%doc %{_docdir}/%{name}/libibverbs.md


%files -n libibverbs-utils
%{_bindir}/ibv_*
%{_mandir}/man1/ibv_*


%files -n ibacm
%config(noreplace) %{_sysconfdir}/rdma/ibacm_opts.cfg
%{_bindir}/ib_acme
%{_sbindir}/ibacm
%{_unitdir}/ibacm.service
%{_unitdir}/ibacm.socket
%dir %{_libdir}/ibacm
%{_libdir}/ibacm/*
%doc %{_docdir}/%{name}/ibacm.md
%{_mandir}/man1/ib_acme.*
%{_mandir}/man7/ibacm.*
%{_mandir}/man7/ibacm_prov.*
%{_mandir}/man8/ibacm.*


%files -n iwpmd
%config(noreplace) %{_sysconfdir}/rdma/modules/iwpmd.conf
%config(noreplace) %{_sysconfdir}/iwpmd.conf
%{_sbindir}/iwpmd
%{_unitdir}/iwpmd.service
%{_udevrulesdir}/90-iwpmd.rules
%{_mandir}/man8/iwpmd.*
%{_mandir}/man5/iwpmd.*


%files -n libibumad
%{_libdir}/libibumad*.so.*


%files -n librdmacm
%{_libdir}/librdmacm*.so.*
%dir %{_libdir}/rsocket
%{_libdir}/rsocket/*.so*
%doc %{_docdir}/%{name}/librdmacm.md
%{_mandir}/man7/rsocket.*


%files -n librdmacm-utils
%{_bindir}/cmtime
%{_bindir}/mckey
%{_bindir}/rcopy
%{_bindir}/rdma_client
%{_bindir}/rdma_server
%{_bindir}/rdma_xclient
%{_bindir}/rdma_xserver
%{_bindir}/riostream
%{_bindir}/rping
%{_bindir}/rstream
%{_bindir}/ucmatose
%{_bindir}/udaddy
%{_bindir}/udpong
%{_mandir}/man1/cmtime.*
%{_mandir}/man1/mckey.*
%{_mandir}/man1/rcopy.*
%{_mandir}/man1/rdma_client.*
%{_mandir}/man1/rdma_server.*
%{_mandir}/man1/rdma_xclient.*
%{_mandir}/man1/rdma_xserver.*
%{_mandir}/man1/riostream.*
%{_mandir}/man1/rping.*
%{_mandir}/man1/rstream.*
%{_mandir}/man1/ucmatose.*
%{_mandir}/man1/udaddy.*
%{_mandir}/man1/udpong.*


%files -n srp_daemon
%config(noreplace) %{_sysconfdir}/srp_daemon.conf
%config(noreplace) %{_sysconfdir}/rdma/modules/srp_daemon.conf
%{_libexecdir}/srp_daemon/start_on_all_ports
%{_unitdir}/srp_daemon.service
%{_unitdir}/srp_daemon_port@.service
%{_sbindir}/ibsrpdm
%{_sbindir}/srp_daemon
%{_sbindir}/run_srp_daemon
%{_udevrulesdir}/60-srp_daemon.rules
%{_mandir}/man5/srp_daemon.service.5*
%{_mandir}/man5/srp_daemon_port@.service.5*
%{_mandir}/man8/ibsrpdm.8*
%{_mandir}/man8/srp_daemon.8*
%doc %{_docdir}/%{name}/ibsrpdm.md


%changelog
* Wed Aug 02 2023 cunshunxia <cunshunxia@tencent.com> - 47.0-1
- upgrade to 47.0

* Tue Jul 11 2023 Xiaojie Chen <jackxjchen@tencent.com> - 46.0-1
- Upgrade to upstream version 46.0

* Mon May 22 2023 Xiaojie Chen <jackxjchen@tencent.com> - 45.0-3
- Move libmana.so.1* to subpackage libibverbs

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 45.0-2
- Rebuilt for OpenCloudOS Stream 23.05

* Thu Apr 27 2023 Xiaojie Chen <jackxjchen@tencent.com> - 45.0-1
- Update to version 45.0

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 40.0-2
- Rebuilt for OpenCloudOS Stream 23

* Mon Jun 06 2022 rockerzhu <rockerzhu@tencent.com> - 40.0-1
- Initail build
